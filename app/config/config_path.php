<?php

/**
 * Configuration
 *
 */

define('DOCUMENT_PATH', '/var/www/sendfree/');
define('BASE_HREF', 'http://dev.sendfree.com/');
define('JS_PATH', BASE_HREF.'app/assets/js/');
define('CSS_PATH', BASE_HREF.'app/assets/css/');
define('IMAGE_PATH', BASE_HREF.'app/assets/images/');


 error_reporting(E_ALL);
 ini_set("display_errors", 1);

/**
 * Set the default time-zone
 */
date_default_timezone_set('Asia/Kolkata');


//require_once CONFIG_PATH . 'db_config.php';
?>