<?php

class MessagesController extends BaseController {

    /**
     * Show the form for creating a new link.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('messages.message_create');
    }

    /**
     * Store a newly created message in storage.
     */
    public function store()
    {
        // Validate input
        $v = Message::validate(Input::all());
      
        $url_list=array();
        $msg = new Message;

        //Get Encrypted Message
        $key = mt_rand(1000000000000,9000000000000);
        $message=Message::clean_message(Input::get('message'),$key);
       
        // Save the message to the database
        $msg->message = $message;
        $msg->key = $key;
        $msg->name = strip_tags(Input::get('name'));
        $msg->email = strip_tags(Input::get('email'));
        $msg->save();

        // This condition is true when the user replies
        if( Input::has('reply') )
            $limit=1; 
        else 
            $limit=(strip_tags(Input::get('number'))>3) ? 3 :strip_tags(Input::get('number')) ;

        $temp_expiry=strip_tags(Input::get('expiry'));
        $expiry_days= ( $temp_expiry==7 || $temp_expiry==14 || $temp_expiry==21 )  ? $temp_expiry : 7;
        
        //Generate Multiple Link    
        for($i=0;$i<$limit;$i++) {
    
            $url = new Url;
            //url will be like http://dev.sendfree.com/unique_url/unique_url_to_show 

            $unique_url=Url::get_unique_url();
            $url->url = $unique_url;
            
            $unique_url_to_show=Url::get_url_to_show();
            $url->url_to_show = $unique_url_to_show;

            $url->msg_id=$msg->id;
            $url->expiry_day=$expiry_days;
            $url->active=1;
            
            $url->save();

            $link_formed=$unique_url."/".$unique_url_to_show;
            array_push($url_list,$link_formed);
        }

        //If Replied Then Send The Link To The Corrosponding Email
        if( Input::has('reply') && strip_tags(Input::has('reply')) ==1 )
        {
             $reply_obj=new MessagesController();
             $status=$reply_obj->reply($expiry_days,$link_formed,$message);   
             return ($status==1) ? ( View::make('messages.reply_successful')) : (View::make('messages.reply_unsuccessful')) ;  
        }

        //Return the Links Generated 
        return View::make('messages.show_link', ['url' => $url_list,'expiry'=>$expiry_days]);
    }

    /**
     * Reply to the Message.
    */
    public function reply($expiry_days,$link_formed,$message){

            $u=explode("/",strip_tags(Input::get('url')));
            // Fetch the EmailId to reply 
            $url = Url::where('url', '=', $u[1])
                ->where('url_to_show', '=', $u[2])
                ->first();
            
            //Return Index Page if Any Random URL is submitted via Curl and that requets passes via csrf filter and ajax filter because
            //that's a possibilty as both can be bypassed   
            if(is_null($url)){
                return View::make('messages.message_create');
                exit;
            }
            
            $msg  = Message::find($url->msg_id);
            $email= $msg->email;    
            $name =$msg->name?$msg->name:"";
            $expiry=$expiry_days;
            
            Mail::send('emails.reply', array('link' => $link_formed,'name'=>$name,'expiry'=>$expiry), function($message) use ($email,$name) {
                $message->to($email,$name)->subject('You Got A Reply to the Link You Shared Via Share-A-Link!!');
            });

            if(count(Mail::failures()) > 0)
                return 0;//return View::make('messages.reply_unsuccessful');
            else{
               return 1;View::make('messages.reply_successful');
            }
    }

    /**
     * Display the Message.
     */
    public function show($url, $url_to_show)
    {
        $reply_type=false;
        $message_type=0; // if not message found then return 0 else return 1 
        $name="";
        // Fetch our message url 
        $url = Url::where('url', '=', $url)
                ->where('url_to_show', '=', $url_to_show)
                ->first();

        if(is_null($url) or !($url->active)) {
            
            //Link is Invalid
            return View::make('messages.message_lost');
        } else {
            //Link is Validated
            //Now Check if it Expired or Not        
            $st1 = strtotime($url->created_at);
            $st2 = time();
            $diff=$st2-$st1;
                    
                if(!($diff > ( 60*60*24*$url->expiry_day )))
                {
                    //Set the Message InActive As It's Already 
                    $msg_id=$url->msg_id;
                    $url->active=0;
                    $url->save();

                    $msg = Message::find($msg_id);
                    $key= $msg->key;
                    $decoded = base64_decode($msg->message);
                    $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_ECB));
                    
                    $message=html_entity_decode($decrypted, ENT_QUOTES | ENT_IGNORE, "UTF-8");
                    if($msg->name)
                       $name=$msg->name;
                    else
                       $name="";
                    $message_type=1;

                    if(!empty($msg->email)){
                        $reply_type=true;
                    }

                }
                else{
                   ///if messgae expired deactive it 
                   $msg_id=$url->msg_id;
                   $url->active=0;
                   $url->save(); 
                   return View::make('messages.message_lost');
                }
        }
        return View::make('messages.message_show', ['name' => $name,'msg'=>$message,'message_type'=>$message_type,'reply_type'=>$reply_type]);
    }
}
