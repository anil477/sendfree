<?php
 
/**
 * @author anil
 */
cleanUp::deltedOldMessage();
class cleanUp{

  	//delete old expired message
    public static function deltedOldMessage() {

       $url=Url::all();
       foreach ($url as $key => $value) {
        	
        	$st1 = strtotime($value->created_at);
            $st2 = time();
            $diff=$st2-$st1;

        	if(($value->active==0) or ($diff > ( 60*60*24*$value->expiry_day ))){
        		 Url::destroy($value->id);
        		 Message::destroy($value->msg_id);
        	} 
        } 
      
    }

}//class ends here 

//php artisan command:make cleanup
