$(document).ready(function() {

          Base_href="http://dev.sendfree.com/";
          $("#show_form").hide();
          $("#get_link").submit(function() {
  
                $('button[type=submit], input[type=submit]').attr('disabled',true);                    
                var name=    $('#name').val(); 
                var email=   $('#email').val();
                var message= $('#message').val();
                var token =  $("input[name=_token]").val();
                var number = $("#number option:selected").val();
                var expiry = $("#expiry option:selected").val();

                var data="name=" + name + "&email=" + email + "&message=" + message + "&_token=" + token + "&number=" + number + "&expiry=" + expiry;   

                $.ajax({
                     type: "POST",
                     url: Base_href+"getlink",   //replace this hardcoded value
                     data: data, // serializes the form's elements.
                     success: function(data)
                     {
                        $('.unique_link').html(data); 
                        $("#get_link").remove();
                     }
                });
          return false; // avoid to execute the actual submit of the form.
          }); //form submit close
    
          //open the index page if the user clicks on more link
          $("#more_link").click(function(){
              window.open(Base_href,"_self");
          });
          
          //handle click on reply
          $("#reply").click(function(){
               $("#show_form").show(); 
               $("#remove").remove();           
          });

          $("#get_link_reply").submit(function() {

                
                $('button[type=submit], input[type=submit]').attr('disabled',true);          
                var name=    $('#name').val(); 
                var email=   $('#email').val();
                var message= $('#message').val();
                var token =  $("input[name=_token]").val();
                var url = location.pathname;
                var expiry = $("#expiry option:selected").val();

                var data="name=" + name + "&email=" + email + "&message=" + message + "&_token=" + token + "&reply=1" + "&url=" + url + "&expiry=" + expiry;   
                $('#show_form').remove();
                $("#loading").show();
                $.ajax({
                     type: "POST",
                     url: Base_href+"submit_reply_form",   //replace this hardcoded value
                     data: data, // serializes the form's elements.
                     success: function(data)
                     {
                        $("#msg_show_footer").html(data);                      
                        $("#loading").remove();
                     }
                 });
              return false; // avoid to execute the actual submit of the form.
           });//form submit close
        
        $(".copy-button").zclip({
        path: "http://dev.sendfree.com/app/assets/js/ZeroClipboard.swf",
        copy: function(){
          //alert("sas");
            return $(".show_ans").text();
            }
        });
      
}); //document ready close


