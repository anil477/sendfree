<?php

Route::get('/', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
Route::post('/getlink', ['as' => 'messages.store','before' => array('csrf','ajax'), 'uses' => 'MessagesController@store']);
Route::get('/{url}/{url_to_show}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
Route::get('/how-it-works', function()
{
	return View::make('layouts.how_it_works');		
});
Route::get('/tech', function()
{
	return View::make('layouts.tech');		
});
Route::post('/submit_reply_form', ['as' => 'messages.show_reply_form','before' => array('csrf', 'ajax','validate'), 'uses' => 'MessagesController@store']);
