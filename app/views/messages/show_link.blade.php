
	<h2>{{ (count($url)>1) ? 'Here Are Your Links': "Here's Your Link" }}.Go Ahead And Share!!</h2>
		
		<p></p>
	    @foreach ($url as $x)
			<div class="bs-callout bs-callout-success">
				<p class="show_ans"> {{ BASE_HREF }}{{ $x }} </p>
				<!-- <button class="copy-button">COPY</button> -->
			</div>
		@endforeach
		 
		 <button type="button" class="btn btn-default" onClick="window.location.href=window.location.href">I Need More Links!!</button>
		 <p id="expire_text">Link expires in {{ $expiry }} Days.</p>