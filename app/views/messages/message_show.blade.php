 @extends('layouts/master')
 @section('container')
<div id="msg_show_footer">
			<div id="remove">		
							@if ($message_type =='0')
								    <div class="bs-callout bs-callout-success">
										<p> {{ $msg }} </p>		        
									</div>
							@endif

							@if ($message_type =='1')
								    <h2>Here's Your Message!! {{ (!empty($name)) ? 'From '.$name : '' }}</h2>
									<p></p>
									<div class="bs-callout bs-callout-success">
										<p id="msg"> {{ $msg }} </p>		        
									</div>
							@endif
				@if($reply_type)	
					<button type="button" class="btn btn-default" id="reply">I Wanna Reply!!</button>
				@endif
	    		<button type="button" class="btn btn-default" id="more_link">Get Links Here!!</button>
			</div>
		<div id="loading" style="display:none;" align="center"><img src="{{ IMAGE_PATH }}ajax_loader.gif"></div>		
		@if($reply_type)
			<div id="show_form">		
					{{ Form::open(['method' => 'post','class' => 'form','id'=>'get_link_reply']) }}									
			    <div class="form-group">
					<input type="text" class="form-control input-lg" placeholder="Name" name="name" id="name">    
			   	</div> 
			    
			    <div class="form-group">	
					<select id="expiry">
					  <option value="7">Select In How Many Days The Message Expires</option>
					  <option value="7">7 Days</option>
					  <option value="14">14 Days</option>
					  <option value="21">21 Days</option>
					</select>
				</div>
			    <div class="form-group">
			        <input type="email" class="form-control input-lg" placeholder="Email:The Receiver Can Reply You Back Via Email" name="email" id="email">
			    </div>
			    <div class="form-group">
			        <textarea type="text" class="form-control input-lg search-query" rows="2" placeholder="{{ $msg }}" name="message" id="message" maxlength="160" required></textarea> 
			    </div>
			    <div class="form-group">
			        <button class="btn btn-primary btn-lg btn-block" type="submit">Reply!!</button>
			    </div>
					{{ Form::close() }}				
		    </div>
		@endif	
</div>		
@endsection