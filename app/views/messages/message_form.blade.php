<div id="footer" class="container msg_form">

<div class="unique_link">
</div>
	
	{{ Form::open(['method' => 'post','class' => 'form col-md-8','id'=>'get_link']) }}									
	    <div class="form-group">
			<input type="text" class="form-control input-lg" placeholder="Name" name="name" id="name" >    
	    </div>

	    <div class="form-group">
				<select id="number" class="form-control input-lg">
				  <option value="1" selected>Select The Number Of Links You Want</option>
				  <option value="1">1 Link</option>
				  <option value="2">2 Links</option>
				  <option value="3">3 Links</option>
				</select>
		</div>	
		<div class="form-group" >	
				<select id="expiry" class="form-control input-lg">
				  <option value="7" selected>Select In How Many Days The Message Expires</option>
				  <option value="7">7 Days</option>
				  <option value="14">14 Days</option>
				  <option value="21">21 Days</option>
				</select>
		</div> 
	    
	    <div class="form-group">
	        <input type="email" class="form-control input-lg" placeholder="Email:The Receiver Can Reply You Back Via Email" name="email" id="email">
	    </div>
	    <div class="form-group">
	        <textarea type="text" class="form-control input-lg search-query" rows="2" placeholder="Your Message In 160 Charcaters Here:Only Message Is Mandatory" name="message" id="message" maxlength="160" required></textarea> 
	    </div>
	    <div class="form-group">
	        <button class="btn btn-primary btn-lg btn-block" type="submit">Done</button>
	    </div>
	{{ Form::close() }}				


</div>
		