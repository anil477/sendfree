
<h3>Hi {{ $name }},</h3>
  You got a reply to a link you shared via Share-A-Link.in
  Here it is {{ BASE_HREF }}{{ $link }}. Be Quick to check it out as it expires in {{ $expiry }} days.
<br>Cheers,
<br>ShareALink.
	<br>{{ BASE_HREF }}  