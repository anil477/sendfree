<!DOCTYPE HTML>

<html>
	<head>
		<title>Share-A-Link</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta http-equiv="expires" content="Sun, 01 Jan 2014 00:00:00 GMT"/>
		<meta http-equiv="pragma" content="no-cache" />		
		<meta name="description" content="Get Unique Link for your Message.Share these link with anyone.">
		<meta charset="UTF-8">
		
		{{ HTML::script( JS_PATH.'jquery.min.js') }}
		{{ HTML::script( JS_PATH.'bootstrap.min.js') }}
		{{ HTML::script( JS_PATH.'jquery.clipboard.js') }}
    	{{ HTML::script( JS_PATH.'index.js') }} 
		{{-- HTML::script( JS_PATH.'analytics.js') --}} 
		{{ HTML::style(CSS_PATH.'bootstrap.min.css') }}
		{{ HTML::style(CSS_PATH.'bootstrap-theme.min.css') }}
		{{ HTML::style(CSS_PATH.'snippet.css') }}
		{{ HTML::style(CSS_PATH.'skel.css') }}
		{{ HTML::style(CSS_PATH.'style.css') }}

		<link rel="icon" type="image/ico" href="{{ IMAGE_PATH }}ss.ico">
	
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	</head>
	<body>

		<!-- Header -->
			<div id="header-wrapper">
				<div id="header" class="container">
					
					<!-- Logo -->
						<h1 id="logo" class="heading" data-gtmcategory="Heading" data-gtmaction="Click" data-gtmlabel="{{ $_SERVER['REQUEST_URI'] }}"><a href="{{ BASE_HREF }}" class="heading" data-gtmcategory="Heading" data-gtmaction="Click" data-gtmlabel="{{ $_SERVER['REQUEST_URI'] }} ">Share-A-Link</a></h1>
					<!-- Nav -->
						<nav id="nav">
							<ul>
								<li class="stack" data-gtmcategory="Stack" data-gtmaction="Click" ><a href="/tech" class="stack" data-gtmcategory="Stack" data-gtmaction="Click">Stack</a></li>
								<li class="break stack" data-gtmcategory="Stack" data-gtmaction="Click"><a href="/how-it-works" class="what" data-gtmcategory="What-Is-This" data-gtmaction="Click">What's This</a></li>
							</ul>
						</nav>

				</div>
			</div>


		<!-- Container Starts -->
			<div id="footer-wrapper">
	
				@yield('container')


		<!-- Container Ends-->	
			</div>	<!-- Trailing closing div from the -->	
				<!-- <div id="copyright" class="container">
					<ul class="menu">
					<li>&copy; Share-A-Link.All rights reserved.</li><li>More: <a href="/how-it-works">How It Works</a></li>
					</ul>
				</div> -->
	</body>
</html>
