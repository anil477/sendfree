<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Url extends Eloquent {
	
	public $timestamps=true;
	protected $fillable =array('msg_id','url','active','url_to_show');

	public static function  get_unique_url() {

 		// set a random number
 		$number = rand(10000, 9999999);

        // character list for generating a random string
		$charlist = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

			$decimal = intval($number);

			//get the list of valid characters
			$charlist = substr($charlist, 0, 62);

			$converted = '';

            while($number > 0) {
				$converted = $charlist{($number % 62)} . $converted;
				$number = floor($number / 62);
			}

            if( static::whereUrl($converted)->first() ) {
				$converted = static::get_unique_url();
			}

            return $converted;
	}

	public static function get_url_to_show(){
	
		$random = substr( md5(rand()), 0, 7);
		if( static::whereUrlToShow($random)->first() ) {
				$random = static::get_url_to_show();
			}
		return $random;	
	}

}
