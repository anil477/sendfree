<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Message extends Eloquent {
	
	public static $rules = array(
                'name'    => 'Max:30|Alpha',
                'email'   => 'Between:3,50|Email',
                'number'  => 'Between:1,3',
                'expiry'  => 'Between:7,21',
                'message' => 'Max:160|Alpha'
        );
	public $timestamps=false;
	protected $fillable =array('message','key','email','name');

	public static function validate($input)
	{
		$v = Validator::make($input, static::$rules);
		return $v->fails() ? $v : true;
	}

	public static function clean_message($msg,$key){

		$clean_msg=htmlentities(strip_tags($msg), ENT_QUOTES, 'UTF-8');
        $passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key,$clean_msg, MCRYPT_MODE_ECB);
        $message = base64_encode($passcrypt);
        return $message;
	}

	public static function  get_unique_url() {

 		// set a random number
 		$number = rand(10000, 9999999);

        // character list for generating a random string
		$charlist = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

			$decimal = intval($number);

			//get the list of valid characters
			$charlist = substr($charlist, 0, 62);

			$converted = '';

            while($number > 0) {
				$converted = $charlist{($number % 62)} . $converted;
				$number = floor($number / 62);
			}

            if( static::whereUrl($converted)->first() ) {
				$converted = static::get_unique_url();
			}

            return $converted;
	}


}
